import pgzrun
import sys
import os

TITLE = 'Midday Shootout'
WIDTH = 512
HEIGHT = 768
ICON = 'images/icon.png'


def draw():
    screen.clear()
    if game.main_menu:
        game.draw_menu()
    else:
        game.draw_game()
        game.draw_hit_animation()
        game.draw_score()
        game.draw_pause_menu()
        game.draw_game_over()



def update():
    if game.main_menu:
        game.update_menu()
    elif not game.paused:
        game.update_game()
        game.update_hit_animation()
        game.update_highscore()


def on_key_down(key):
    if game.main_menu:
        game.menu_selection(key)
    else:
        if not game.game_over:
            game.update_pause_menu(key)
            if not game.paused and not game.show_intro:
                game.player_fire(key)
        else:
            game.update_game_over(key)


class MiddayShootout:
    def __init__(self):
        self.setup_menu()


    def setup_player(self):
        self.player = Actor('player_standby')
        self.player.y_limits = HEIGHT - 35, int(HEIGHT/2) + 25
        self.player.bottomright = WIDTH, max(self.player.y_limits)
        self.player.health = WIDTH
        self.player.bullets = []
        self.player.speed = 2
        self.player.bullet_speed = 10
        self.player.alive = True
        self.player.bloodsplats = []
    

    def setup_enemy(self):
        self.enemy = Actor('enemy_standby')
        self.enemy.y_limits = 35, HEIGHT/2 - 25
        self.enemy.topleft = 20, min(self.enemy.y_limits)
        self.enemy.health = WIDTH
        self.enemy.bullets = []
        self.enemy.x_direction = 1
        self.enemy.y_direction = 1
        self.enemy.vision = []
        self.enemy.vision_zones = int(WIDTH / self.player.width) + 1
        self.enemy.alive = True
    

    def move_player(self):
        if keyboard.up:
            self.player.y -= 2
        elif keyboard.down:
            self.player.y += 2
        elif keyboard.right:
            self.player.x += 2
        elif keyboard.left:
            self.player.x -= 2
        
        self._check_boundary()
    
    def _check_boundary(self):
        if self.player.right > WIDTH:
            self.player.right = WIDTH
        elif self.player.left < 0:
            self.player.left = 0

        if self.player.top < min(self.player.y_limits):
            self.player.top = min(self.player.y_limits)
        elif self.player.bottom > max(self.player.y_limits):
            self.player.bottom = max(self.player.y_limits)
    

    def player_fire(self, key):
        if key == keys.SPACE and len(self.player.bullets) < 3:
            bullet = Actor('player_bullet')
            bullet.midbottom = self.player.left + 28, self.player.top
            sounds.laser1.play()
            self.player.bullets.append(bullet)
    

    def draw_health(self):
        player_bar = Rect(0, HEIGHT - 20, self.player.health, 20)
        screen.draw.filled_rect(player_bar, (200, 10, 10))
        enemy_bar = Rect(0, 0, self.enemy.health, 20)
        screen.draw.filled_rect(enemy_bar, (200, 10, 10))
    

    def update_bullets(self):
        self._update_player_bullets()
        self._update_enemy_bullets()


    def move_enemy(self):
        if self.enemy.top >= min(self.enemy.y_limits) and self.enemy.bottom <= max(self.enemy.y_limits):
            if self.enemy.right <= WIDTH and self.enemy.left >= 0:
                self.enemy.x += self.enemy.speed * self.enemy.x_direction
            else:
                if self.enemy.right > WIDTH:
                    self.enemy.right = WIDTH
                elif self.enemy.left < 0:
                    self.enemy.left = 0
                self.enemy.x_direction *= -1
                self.enemy.y += self.enemy.speed * self.enemy.y_direction
        else:
            if self.enemy.top < min(self.enemy.y_limits):
                self.enemy.top = min(self.enemy.y_limits)
            elif self.enemy.bottom > max(self.enemy.y_limits):
                self.enemy.bottom = max(self.enemy.y_limits)
            self.enemy.y_direction *= -1
    

    def make_vision_zones(self):
        for i in range(self.enemy.vision_zones):
            zone = Rect(self.player.width * i, 0, self.player.width, HEIGHT)
            self.enemy.vision.append(zone)
    

    def check_zones(self):
        for zone in self.enemy.vision:
            if self.player.colliderect(zone) and not self.enemy.colliderect(zone):
                self.move_towards_player(zone)
            elif self.player.colliderect(zone) and self.enemy.colliderect(zone):
                self.enemy_fire()
                if self.enemy.alive and self.player.alive and not self.paused:
                    self.enemy.y += self.enemy.speed * self.enemy.y_direction
    

    def move_towards_player(self, zone):
        if zone.left > self.enemy.right:
            self.enemy.x_direction = 1
        elif zone.right < self.enemy.left:
            self.enemy.x_direction = -1
    

    def enemy_fire(self):
        if len(self.enemy.bullets) < 3:
            bullet = Actor('enemy_bullet')
            bullet.midtop = self.enemy.right - 28, self.enemy.bottom - 15
            sounds.laser5.play()
            self.enemy.bullets.append(bullet)
    

    def _update_player_bullets(self):
        for bullet in self.player.bullets:
            bullet.y -= self.player.bullet_speed

            if bullet.y < 0:
                self.player.bullets.remove(bullet)
                self.reset_combo = True
                self.update_score()
            elif self.enemy.colliderect(bullet):
                self.enemy.health -= self.player.bullet_damage
                self.enemy.y -= 3
                self.update_score()
                self.hit_animation.show_hit = True
                sounds.metal_hit.play()
                self.player.bullets.remove(bullet)
    

    def _update_enemy_bullets(self):
        for bullet in self.enemy.bullets:
            bullet.y += self.enemy.bullet_speed

            if bullet.y > HEIGHT:
                self.enemy.bullets.remove(bullet)
            elif self.player.colliderect(bullet):
                self.player_hit()
                self.update_score()
                self.enemy.bullets.remove(bullet)


    def animate(self):
        for zone in self.enemy.vision:
            if self.player.colliderect(zone) and not self.enemy.colliderect(zone):
                self.enemy.image = 'enemy_standby'
            elif self.player.colliderect(zone) and self.enemy.colliderect(zone):
                self.enemy.image = 'enemy_shoot'
        
        if keyboard.space:
            self.player.image = 'player_shoot'
        else:
            self.player.image = 'player_standby'
    

    def draw_bullets(self):
        for bullet in self.player.bullets:
            bullet.draw()
        for bullet in self.enemy.bullets:
            bullet.draw()


    def check_health(self):
        if self.player.health <= 0:
            self.player.alive = False
            self.game_over = True
            if self.play_result:
                sounds.you_lose.play()
                self.play_result = False
        else:
            self.player.alive = True

        if self.enemy.health <= 0:
            self.enemy.alive = False
            self.game_over = True
            if self.play_result:
                sounds.you_win.play()
                self.play_result = False
        else:
            self.enemy.alive = True
    

    def draw_result(self):
        if not self.player.alive:
            result = 'You Lose!'
            pos = (int(WIDTH/2), int(HEIGHT * 3/4))
        elif not self.enemy.alive:
            result = 'You Win!'
            pos = (int(WIDTH/2), int(HEIGHT/4))
        screen.draw.text(result, center=pos, fontname='permanent_marker', fontsize=70)


    def player_hit(self):
        self.player.health -= self.enemy.bullet_damage
        self.player.y += 3
        self.penalty = True
        self.reset_combo = True
        sounds.hit.play()
        bloodsplat = Actor('bloodsplat')
        bloodsplat.center = self.player.center
        self.player.bloodsplats.append(bloodsplat)
    

    def draw_bloodspats(self):
        for bloodsplat in self.player.bloodsplats:
            bloodsplat.draw()
    

    def draw_game(self):
        screen.blit('background', (0, 0))
        if self.show_intro:
            self.draw_intro()
        elif not self.show_intro:
            game.draw_bloodspats()
            if game.player.alive:
                game.player.draw()
            else:
                game.draw_result()
            
            if game.enemy.alive:
                game.enemy.draw()
            else:
                game.draw_result()

            if game.player.alive and game.enemy.alive:
                game.draw_bullets()
                game.draw_health()


    def update_intro(self):
        if self.show_intro:
            self.intro_count -= 1
            if self.intro_count == 2:
                self.intro_message = str(self.intro_count)
                sounds.two.play()
            elif self.intro_count == 1:
                self.intro_message = str(self.intro_count)
                sounds.one.play()
            elif self.intro_count == 0:
                self.intro_message = 'Fight!'
                sounds.fight.play()
            else:
                self.show_intro = False
    
    
    def draw_intro(self):
        pos = (int(WIDTH/2), int(HEIGHT/4))
        fontname = 'permanent_marker'
        screen.draw.text(self.intro_message, center=pos, fontname=fontname, fontsize=70)
    

    def setup_intro(self):
        self.show_intro = True
        self.intro_count = 3
        self.intro_message = str(self.intro_count)
        sounds.three.play()
        clock.schedule_interval(self.update_intro, 1)


    def update_game(self):
        if game.player.alive and game.enemy.alive and not game.show_intro:
            game.move_player()
            game.move_enemy()
            game.update_bullets()
            game.animate()
        game.check_health()
    

    def draw_menu(self):
        screen.blit(self.interface, (0, 0))
        if not self.help_screen:
            self.cursor.draw()
    

    def update_menu(self):
        if not self.difficulty_screen and not self.help_screen:
            if self.selected_button == 'play':
                self.difficulty_screen = True
                self.setup_difficulty()
            elif self.selected_button == 'help':
                self.help_screen = True
                self.setup_help()
            elif self.selected_button == 'exit':
                sys.exit()
        elif self.difficulty_screen:
            if self.selected_button in self.menu_buttons:
                self.main_menu = False
                self.setup_game()
        elif self.help_screen:
            if self.selected_button == 'play':
                self.help_screen = False
                self.difficulty_screen = True
                self.cursor.y = self.menu_buttons['play']
                self.setup_difficulty()
    

    def setup_game(self):
        if not self.main_menu:
            self.setup_intro()
            self.paused = False
            self.game_over = False
            self.save_file = 'highscore.txt'
            self.setup_ingame_menu_buttons()
            self.setup_score()
            self.setup_player()
            self.setup_enemy()
            self.setup_hit_animation()
            self.select_difficulty()
            self.make_vision_zones()
            clock.schedule_interval(self.check_zones, 0.15)
            music.play('bg_music')
            music.set_volume(0.3)
            self.play_result = True


    def setup_menu(self):
        self.main_menu = True
        self.interface = 'main-menu'
        self.difficulty_screen = False
        self.help_screen = False
        self.cursor = Actor('menu-cursor')
        self.menu_buttons = {'play': 275, 'help': 380, 'exit': 485}
        self.cursor.midleft = 110, self.menu_buttons['play']
        self.selected_button = ''
        music.play('menu_music')
        music.set_volume(0.3)
    

    def menu_selection(self, key):
        if not self.difficulty_screen and not self.help_screen:
            if key == keys.DOWN:
                if self.cursor.y == self.menu_buttons['play']:
                    self.cursor.y = self.menu_buttons['help']
                elif self.cursor.y == self.menu_buttons['help']:
                    self.cursor.y = self.menu_buttons['exit']
            elif key == keys.UP:
                if self.cursor.y == self.menu_buttons['exit']:
                    self.cursor.y = self.menu_buttons['help']
                elif self.cursor.y == self.menu_buttons['help']:
                    self.cursor.y = self.menu_buttons['play']
            elif key == keys.RETURN:
                for button, position in self.menu_buttons.items():
                    if self.cursor.y == position:
                        self.selected_button = button
        elif self.difficulty_screen:
            if key == keys.DOWN:
                if self.cursor.y == self.menu_buttons['normal']:
                    self.cursor.y = self.menu_buttons['hard']
                elif self.cursor.y == self.menu_buttons['hard']:
                    self.cursor.y = self.menu_buttons['insane']
            elif key == keys.UP:
                if self.cursor.y == self.menu_buttons['insane']:
                    self.cursor.y = self.menu_buttons['hard']
                elif self.cursor.y == self.menu_buttons['hard']:
                    self.cursor.y = self.menu_buttons['normal']
            elif key == keys.RETURN:
                for button, position in self.menu_buttons.items():
                    if self.cursor.y == position:
                        self.selected_button = button
        elif self.help_screen:
            if key == keys.RETURN:
                self.selected_button = 'play'


    def setup_difficulty(self):
        if self.difficulty_screen:
            self.interface = 'difficulty_menu'
            self.menu_buttons = {'normal': 275, 'hard': 380, 'insane': 485}
    

    def select_difficulty(self):
        if self.selected_button == 'normal':
            self.player.bullet_damage = 20
            self.enemy.bullet_speed = 10
            self.enemy.bullet_damage = 20
            self.enemy.speed = 2
        elif self.selected_button == 'hard':
            self.player.bullet_damage = 15
            self.points = 150
            self.penalty_points = 50
            self.enemy.bullet_speed = 10
            self.enemy.bullet_damage = 25
            self.enemy.speed = 3
        elif self.selected_button == 'insane':
            self.player.bullet_damage = 10
            self.points = 200
            self.penalty_points = 75
            self.enemy.bullet_speed = 13
            self.enemy.bullet_damage = 30
            self.enemy.speed = 4
    

    def setup_help(self):
        self.interface = 'help_screen'
    

    def setup_score(self):
        self.score = 0
        self.points = 100
        self.penalty = False
        self.penalty_points = 25
        self.combo = 0
        self.reset_combo = False
        self.new_highscore = False
        self.get_highscore()
    

    def update_score(self):
        if not self.reset_combo and not self.penalty:
            self.combo += 1
            self.score += self.points * self.combo
        elif self.reset_combo and not self.penalty:
            self.combo = 0
            self.reset_combo = False
        elif self.reset_combo and self.penalty:
            self.combo = 0
            self.score -= self.penalty_points
            if self.score < 0:
                self.score = 0
            self.reset_combo = False
            self.penalty = False


    def draw_score(self):
        pos = 10, HEIGHT - 22
        fontname = 'permanent_marker'
        score = 'Score: ' + str(self.score)
        screen.draw.text(score, bottomleft=pos, fontname=fontname, fontsize=25)
        pos = WIDTH/2, 16
        highscore = 'Highscore: ' + str(self.highscore)
        screen.draw.text(highscore, midtop=pos, fontname=fontname, fontsize=25)
        if self.combo:
            pos = WIDTH, HEIGHT - 22
            combo = f'HIT x{self.combo}'
            screen.draw.text(combo, bottomright=pos, fontname=fontname, fontsize=30)
        if self.new_highscore:
            pos = WIDTH/2, 50
            screen.draw.text('NEW HIGHSCORE!', midtop=pos, fontname=fontname, fontsize=30)
    

    def setup_hit_animation(self):
        self.hit_animation = Actor('hit1')
        self.hit_animation.show_hit = False
        self.hit_animation.frames = 15
        self.hit_animation.current_frame = 1


    def update_hit_animation(self):
        current = self.hit_animation.current_frame
        frames = self.hit_animation.frames
        if self.hit_animation.show_hit and current <= frames:
            self.hit_animation.image = f'hit{current}'
            self.hit_animation.center = self.enemy.center
            current += 1
        else:
            self.hit_animation.image = 'hit1'
            self.hit_animation.show_hit = False
            current = 1
        self.hit_animation.current_frame = current
    

    def draw_hit_animation(self):
        if self.hit_animation.show_hit:
            self.hit_animation.draw()


    def update_pause_menu(self, key):
        if key == keys.ESCAPE and not self.show_intro:
            if not self.paused:
                self.paused = True
            else:
                self.paused = False
        else:
            if self.paused:
                self.ingame_menu_navigation(key)
        

    def draw_pause_menu(self):
        if self.paused:
            pos = WIDTH/2, HEIGHT/4
            fontname = 'permanent_marker'
            screen.draw.text('Paused', center=pos, fontname=fontname, fontsize=70)
            self.restart_button.draw()
            self.exit_button.draw()
    
    
    def ingame_menu_navigation(self, key):
        if key == keys.RIGHT:
            self.restart_button.image = 'restart'
            self.exit_button.image = 'exit_pressed'
        elif key == keys.LEFT:
            self.restart_button.image = 'restart_pressed'
            self.exit_button.image = 'exit'
        elif key == keys.RETURN:
            clock.unschedule(self.check_zones)
            clock.unschedule(self.update_intro)
            if self.exit_button.image == 'exit_pressed':
                self.setup_menu()
            elif self.restart_button.image == 'restart_pressed':
                self.setup_game()


    def setup_ingame_menu_buttons(self):
        self.restart_button = Actor('restart_pressed')
        self.restart_button.center = WIDTH/2 - 50, HEIGHT/2
        self.exit_button = Actor('exit')
        self.exit_button.center = WIDTH/2 + 50, HEIGHT/2
    

    def update_game_over(self, key):
        if self.game_over:
            self.ingame_menu_navigation(key)
    

    def draw_game_over(self):
        if self.game_over:
            self.restart_button.draw()
            self.exit_button.draw()


    def get_highscore(self):
        if os.path.exists(self.save_file):
            with open(self.save_file) as f:
                self.highscore = int(f.read())
        else:
            self.highscore = 0
  

    def update_highscore(self):
        if self.game_over:
            if self.score > self.highscore:
                self.highscore = self.score
                self.new_highscore = True

                with open(self.save_file, 'w') as f:
                    f.write(str(self.score))


game = MiddayShootout()
pgzrun.go()
