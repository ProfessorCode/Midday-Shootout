# Midday Shootout

## About The Game
This game was made for **PyWeek 33** for which the theme was *'Evil Twin'*. It revolves around a gunfight with an evil android. Have fun!

## Dependencies
You need to install *Pygame Zero* for this game to work. You can do that by typing this command.
```
pip install pgzero
```
**Pygame Zero v1.2.1** or higher is required for certain features to work.

It is also recommended that you have Python 3.8.

## Running The Game
### From Source
```
~$ cd <PathToFolder>/Midday_Shootout
~$ python midday_shootout.py
```
### On Linux
Requires `glibc >= 2.31`. (Ubuntu 20.04 or higher)

1. Extract the ZIP file.
2. Make sure `midday_shootout` is executable by going to the file's *Properties*.
3. Run the executable.

## Known Issues
1. **The game window doesn't fit my screen.** - The game window's dimensions are `512 x 768 px`. If your screen height is smaller than this, it won't fit your screen. If you are running the game from source, you can still make it fit by going to `midday_shootout.py` and changing the `HEIGHT` (line 7) variable. *WARNING: The game itself will work fine but some stuff may be cut off from view. It is recommended to keep it as close to the original dimensions as possible.*

2. **Nothing happens when I click!** - The game doesn't support mouse input. It was designed to be used entirely with a keyboard. Use the arrow keys and *ENTER* to navigate the menu. For instructions on how to play the game, go to the 'HELP' section in the game's main menu.

3. **Can I remap the keybindings?** - This is currently not supported in-game. However, if you are running from source, you could possibly do this by looking for the following (in `midday_shootout.py`) and changing them: `keys.UP`, `keys.DOWN`, `keys.RIGHT`, `keys.LEFT`, `keys.SPACE`, `keyboard.space`, `keys.RETURN` and `keys.ESCAPE`. For more information on how to change them, go [here](https://pygame-zero.readthedocs.io/en/stable/builtins.html#the-keyboard).


## Copyright Attributions
### Fonts
- Permanent Marker - [Google Fonts](https://fonts.google.com/specimen/Permanent+Marker?query=permanent#standard-styles)
- Orbitron (in `help_screen.png`) - [Google Fonts](https://fonts.google.com/specimen/Orbitron?query=orbitron)
### Images
- Characters, background, UI elements and icon - [Kenney](https://kenney.nl/)
- Bloodsplat - [PWL](https://opengameart.org/content/blood-splats)
- Hit Animation - [Sinestesia](https://opengameart.org/content/hit-animation-2-frame-by-frame)
- Bullets - [Bonsaiheldin](https://opengameart.org/content/sci-fi-space-simple-bullets)

### Sounds and Music
- Voice-overs - [Kenney](https://kenney.nl/)
- Gun Effects - [dklon](https://opengameart.org/content/laser-fire)
- Player Hit Sound - [TinyWorlds](https://opengameart.org/content/5-hit-sounds-dying)
- Enemy Hit Sound - [qubodup](https://opengameart.org/content/metal-interactions)
- Background Music - [Patrick De Arteaga](https://patrickdearteaga.com)

## License
This game has been licensed under **GPL v3**. Please refer to `LICENSE` for more information.